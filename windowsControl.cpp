#include "windowsControl.h"

#include <opencv2/opencv.hpp>

#ifdef WIN32
#include <windows.h>
#endif

using namespace cv;

std::map<std::string, bool> Wnds;

namespace winNames
{
    const std::string Contours  = "Contours",
                      Result    = "Result",
                      Card      = "Card",
                      Hough     = "Hough",
                      Threshold = "Threshold";
} // namespace winNames

namespace winShow
{

bool Contours  = false,
     Result    = true,
     Card      = true,
     Hough     = false,
     Threshold = false;

void keyboardParse()
{
#ifdef WIN32
    if (GetAsyncKeyState(VK_LCONTROL))
    {
        Result    ^= GetAsyncKeyState('R');
        Contours  ^= GetAsyncKeyState('C');
        Hough     ^= GetAsyncKeyState('H');
        Threshold ^= GetAsyncKeyState('T');
    }
#endif
}

void openClose(std::string name, bool val)
{
    val ? namedWindow(name) : destroyWindow(name);
}

void openCloseWindows()
{
    openClose(winNames::Result,    Result);
    openClose(winNames::Contours,  Contours);
    openClose(winNames::Card,      Card);
    openClose(winNames::Hough,     Hough);
    openClose(winNames::Threshold, Threshold);
}

void processWindows()
{
    openCloseWindows();
    keyboardParse();
}

void show(Mat img, std::string title, bool controlVariable)
{
   if (controlVariable)
       imshow(title, img);
}

} // namespace winShow


//void WinController::itit()
//{
//    Wnds["Contours"]  = false;
//    Wnds["Result"]    = true;
//    Wnds["Card"]      = true;
//    Wnds["Hough"]     = false;
//    Wnds["Threshold"] = false;
//}

//void WinController::openClose()
//{
//    for (auto& wnd : Wnds)
//        wnd.second ? cv::namedWindow(wnd.first) : cv::destroyWindow(wnd.first);
//}

