#ifndef HOUGHPARAMS_H
#define HOUGHPARAMS_H

class HoughParams
{
public:
    HoughParams() = delete;
    HoughParams(int threshVal, int minLenVal, int maxGapVal, double stepVal) :
        thresh(threshVal),
        minLen(minLenVal),
        maxGap(maxGapVal),
        rho  (stepVal)
    {

    }

    int thresh,
        minLen,
        maxGap;

    double rho;
};

#endif // HOUGHPARAMS_H
