TEMPLATE = app
QT += core gui widgets

QMAKE_CXXFLAGS += -std=gnu++11

SOURCES += main.cpp \
    detector.cpp \
    windowsControl.cpp \
    utils.cpp \
    cardAnalyser.cpp

HEADERS += \
    detector.h \
    utils.h \
    windowsControl.h \
    cannyParams.h \
    houghParams.h \
    threshParams.h \
    geometryParams.h \
    momentParams.h \
    cardAnalyser.h

win32 {
INCLUDEPATH +=   C:\opencv248-buildMinGW32bit\include
LIBS        += -LC:\opencv248-buildMinGW32bit\bin \
               -llibopencv_highgui248 \
               -llibopencv_core248    \
               -llibopencv_imgproc248
}

unix {
LIBS += -L/usr/lib/x86_64-linux-gnu/
LIBS += -lopencv_highgui \
        -lopencv_core    \
        -lopencv_imgproc
}

