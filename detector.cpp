#include "detector.h"
#include "windowsControl.h"

#include <vector>
#include <math.h>

using namespace cv;

Detector::Detector() : Detector(0)
{
}

Detector::Detector(int captureNumber, Size limitSize) :
    momentParams_    (0.2, 0.4),
    cannyParams_     (85, 85, 1),
    //houghParams_     (80, 100, 50, 1.0),
    houghParams_     (120, 150, 50, 1),
    geomParams_      (150, 0.5, 1.6, 0.1),
    threshParams_    (59, 143, 0, 129, 300, 9000),
    limitSize_       (limitSize),
    cardRect_        (Point(150, 150), Size(350, 350/geomParams_.proportions))
{
    capture_.open(captureNumber);

    if (!capture_.isOpened())
    {
        std::cerr << "Can't open camera";
        return;
    }

    updateFrames();
    initTrackbars();
    getFps();

    logoTempls_.push_back(imread("logo_MasterCard.png"));
    logoTempls_.push_back(imread("logo_Visa.png"));
    chipTmpl_ = imread("chip.png");
}

Detector::~Detector()
{
    capture_.release();
}

void Detector::exec()
{
    while (waitKey(fps_) != 'q')
    {
        updateFrames();
//        winShow::processWindows();
//        detect();

        Mat cardImg = getCardImg();

        if (!cardImg.empty())
        {
            imshow("Result", cardImg);
            if (cardAnalyzer_.isCard(cardImg))
            {
                qDebug() << "Card";
                while (waitKey(1) != 'p')
                       ;
            }
            qDebug() << "No card";

        }
    }
}

std::vector<std::vector<Point> > Detector::findContours()
{
    std::vector<std::vector<Point> > contures;
    std::vector<Vec4i> hierarchy;

    cv::findContours(contoursImg_, contures, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_TC89_L1);

    for (unsigned int i = 0; i < contures.size(); ++i)
        approxPolyDP(contures.at(i), contures.at(i), cannyParams_.approx, true);

    std::sort(contures.begin(), contures.end(), comparateContours);

    if (winShow::Contours)
        imshow(winNames::Contours, cannyImg_);

    return contures;
}

std::vector<Vec4i> Detector::findHoughLines()
{
    std::vector<Vec4i> lines;

    HoughLinesP(cannyImg_, lines, houghParams_.rho, CV_PI/180, houghParams_.thresh,
                houghParams_.minLen, houghParams_.maxGap);

    contoursImg_ = cannyImg_.clone();

    for (auto l : lines)
        line(contoursImg_, Point(l[0], l[1]), Point(l[2], l[3]), 255, 1);

    removeExtraContours();

    return lines;
}

bool Detector::isCard(const Mat& cardImg, const double& moment)
{
//    int threshSum = sum(centralGradient(getChipThresh(cardImg), true))[0] / 256;

    if (!geomParams_.isPropInRange(cardImg.cols / cardImg.rows))
        return wrongCardRetFalse("Proportions");

//    else if (abs(SizecardImg - cardRect_.area()) > cardRect_.area()*geomParams_.deltaAreaK)
//        return wrongCardRetFalse("Area");

//    else if (getRectDist(rect, cardRect_) > geomParams_.dist)
//        return wrongCardRetFalse("Position");

//    else if (!momentParams_.isInRange(moment))
//        return wrongCardRetFalse("Moment fail");

//    else if (!threshParams_.isInThresh(threshSum))
//        return wrongCardRetFalse("Chip threshold");

//    else if (!isChipInPlace(cardImg))
//        return wrongCardRetFalse("Chip pos");

    else if (!isLogoInPlace(cardImg))
        return wrongCardRetFalse("Logo");

    return true;
}

void Detector::detect()
{
    std::vector<Vec4i> lines = findHoughLines();
    std::vector<std::vector<Point> > contures = findContours();

    if (!contures.size())
        return;

    std::vector<Point> mainConture = contures[0];
    Rect rect = boundingRect(mainConture);
    Mat cardImg = frame_(rect),
        resImg  = frame_.clone();

    if (isCard(cardImg, moments(mainConture).nu20))
    {
        rectangle(resImg, rect, Scalar(0, 255, 0), 2);

        winShow::show(cardImg, winNames::Card, winShow::Card);

        while (waitKey(1) != 'p')
            ;
    }

    if (winShow::Result)
    {
        rectangle(resImg, cardRect_, Scalar(255, 0, 0), 3);
        drawContours(resImg, contures, -1, Scalar(255, 255, 255)); // -1 for draw all contours

        imshow(winNames::Result, resImg);
    }

    if (winShow::Hough)
    {
        Mat houghImg = frame_.clone();

        for (auto l : lines)
            line(contoursImg_, Point(l[0], l[1]), Point(l[2], l[3]), 255, 1);

        imshow(winNames::Hough, houghImg);
    }
}

Mat Detector::getChipImg(const Mat &cardImg)
{
    int h = cardImg.rows,
        w = cardImg.cols;

    if (h/5 < 1 || w / 10 < 1)
        return cardImg(Rect(Point(1, 1), Point(1, 1)));

    return cardImg(Rect(Point(w/10, h/5), Point(w/3, h*2/3)));
}

void Detector::updateFrames()
{
    capture_ >> frame_;

    if (frame_.empty())
    {
        //ERROR_BOX("Empty frame");
        return;
    }

#if 0
    if (limitSize_.width)
        cv::resize(frame_, frame_, limitSize_);
#endif

    Canny(frame_, cannyImg_, cannyParams_.thresh1, cannyParams_.thresh2, cannyParams_.areaSize);
}

Point Detector::findTemplate(const Mat& cardImg, const Mat& tmplFrame, Size reSizeProp)
{
    Mat corImg,
        tmpl;

    resize(tmplFrame, tmpl, Size(cardImg.cols/reSizeProp.width, cardImg.rows/reSizeProp.height));

    int method = CV_TM_CCOEFF_NORMED;

    matchTemplate(cardImg, tmpl, corImg, method);
    normalize(corImg, corImg, 0, 1, NORM_MINMAX, -1, Mat());

    double minVal = 0.0,
           maxVal = 0.0;
    Point minLoc,
          maxLoc;

    minMaxLoc(corImg, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

    Mat resImg = cardImg.clone();

    Point matchLoc = (method == CV_TM_SQDIFF || method == CV_TM_SQDIFF_NORMED) ?
                minLoc : maxLoc; // minLoc for SQDIFF and maxLoc for any another method

    rectangle(resImg, matchLoc, Point(matchLoc.x + tmpl.cols, matchLoc.y + tmpl.rows), Scalar::all(0), 2, 8, 0);

    imshow("Tmpl", resImg);
    imshow("Tmp imgl", tmpl);

    return minVal == 0 ? matchLoc : Point(0, 0);
}

bool Detector::isLogoInPlace(Mat cardImg)
{
    for (unsigned int i = 0; i < logoTempls_.size(); ++i)
    {
        Point p = findTemplate(cardImg, logoTempls_[i], Size(4, 5.5));

        if (p.x > 2*cardImg.cols/3 && p.y > cardImg.rows/2/* && p.y < 9*cardImg.rows/10*/)
            return true;
    }

    return false;
}

bool Detector::isChipInPlace(Mat cardImg)
{
    Point p[2] { findTemplate(cardImg, chipTmpl_, Size(6.6, 6.75)),
                 findTemplate(cardImg, chipTmpl_, Size(8.4, 6.75)) };

    for (int i = 0; i < 2; ++i)
        if (p[i].x > cardImg.cols/10 && p[i].x < 2*cardImg.cols/10 &&
            p[i].y > cardImg.rows/5  && p[i].y < cardImg.rows/2)
        {
            drawElements(cardImg, Rect(p[i].x, p[i].y, cardImg.cols/(i == 0? 6.6 : 8.4), cardImg.rows/6.75));
            return true;
        }

    return false;
}

void Detector::removeExtraContours()
{
    int xL = valInRange<int>(cardRect_.x - 10,                    0, cannyImg_.cols - 1),
        xR = valInRange<int>(cardRect_.x + cardRect_.width  + 10, 0, cannyImg_.cols - 1),
        yU = valInRange<int>(cardRect_.y - 10,                    0, cannyImg_.rows - 1),
        yD = valInRange<int>(cardRect_.y + cardRect_.height + 10, 0, cannyImg_.rows - 1);

    for (int y = 0; y < contoursImg_.rows; ++y)
        for (int x = 0; x < contoursImg_.cols; ++x)
            if (!(x > xL && x < xR && y > yU && y < yD))
                contoursImg_.at<char>(y, x) = 0;

//    Mat cardContours = centralGradient(contoursImg_(cardRect_), false);
//    imshow("Card contours", cardContours);
}

Mat Detector::centralGradient(Mat img, bool fromCenter)
{
    Point center(img.cols/2, img.rows/2);
    double gradation = 1/pntDist(Point(0, 0), center);

    for (int y = 0; y < img.rows; ++y)
        for (int x = 0; x < img.cols; ++x)
            img.at<unsigned char>(y, x) *= fromCenter ? 1 - gradation * pntDist(Point(x, y), center) :
                                                        gradation * pntDist(Point(x, y), center);

    return img;
}

void Detector::drawElements(Mat img, Rect chipRect)
{
    rectangle(img, chipRect, Scalar(0, 0, 255));
    rectangle(img, Rect(chipRect.x, chipRect.y + chipRect.height + 10, chipRect.width*6.2, 3*chipRect.height/4), Scalar(0, 0, 255));

    imshow("Res", img);
}

Mat Detector::getCardImg()
{
    std::vector<Vec4i> lines;
    HoughLinesP(cannyImg_, lines, houghParams_.rho, CV_PI/180, houghParams_.thresh, houghParams_.minLen, houghParams_.maxGap);

    std::vector<Vec4i> horiz,
                       vert;

    for (Vec4i& line : lines)
    {
        if (closeToHorizontal(getAngle(line)))
        {
            if (line[2] < line[0])
                swapSegmentEnds(line);

            horiz.push_back(line);
        }
        else
        {
            if (line[3] < line[1])
                swapSegmentEnds(line);

            vert.push_back(line);
        }
    }

    std::list<std::pair<Vec4i,Vec4i>> horizPars,
                                      verticalPars;

    processVectors(horiz, horizPars);
    processVectors(vert,  verticalPars);

    bool ready = false;
    std::pair<Vec4i, Vec4i> lr,
                            tb;

    for (std::pair<Vec4i, Vec4i> hP : horizPars)
    {
        double horAngle = getAngle(hP.first);

        for (std::pair<Vec4i, Vec4i> vP : verticalPars)
        {
            double vartAngle = getAngle(vP.first);

            if (std::abs(std::abs(horAngle - vartAngle) - CV_PI / 2) < EPSILON)
            {
                lr = hP;
                tb = vP;

                ready = true;
            }
        }
    }

    if (ready)
    {
        std::vector<Point2f> intersPnts = { intersection(lr.first,  tb.first),
                                            intersection(lr.second, tb.second),
                                            intersection(lr.first,  tb.second),
                                            intersection(lr.second, tb.first) };

        std::vector<Point2f> approx;
        cv::approxPolyDP(Mat(intersPnts), approx, arcLength(Mat(intersPnts), true) * 0.02, true);

        if (approx.size() == 4)
        {
            Point2f center;

            for (unsigned int i = 0; i < intersPnts.size(); i++)
                center += intersPnts[i];

            center *= (1. / intersPnts.size());
            sortCorners(intersPnts, center);

            const double RELATION = 1.6;
            Mat quad = Mat::zeros(200, RELATION * 200, CV_8UC3);

            // Corners of the destination image
            std::vector<Point2f> quad_pts;
            quad_pts.push_back(Point(0, 0));
            quad_pts.push_back(Point(quad.cols, 0));
            quad_pts.push_back(Point(quad.cols, quad.rows));
            quad_pts.push_back(Point(0, quad.rows));

            // Get transformation matrix
            Mat transmtx = getPerspectiveTransform(intersPnts, quad_pts);

            // Apply perspective transformation
            warpPerspective(frame_, quad, transmtx, quad.size());

//            imshow("quad");

            return quad;
        }
    }

    return Mat();
}

void Detector::initTrackbars()
{
    namedWindow(winNames::Threshold);

    createTrackbar("H min", winNames::Threshold, &threshParams_.hMin, 255);
    createTrackbar("H max", winNames::Threshold, &threshParams_.hMax, 255);
    createTrackbar("S min", winNames::Threshold, &threshParams_.sMin, 255);
    createTrackbar("S max", winNames::Threshold, &threshParams_.sMax, 255);
}

void Detector::getFps()
{
    fps_ = capture_.get(CV_CAP_PROP_FPS);

    if (fps_ == -1 || fps_ == 0)
        fps_ = 30;
}

