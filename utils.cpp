#include "utils.h"

#include <opencv2/opencv.hpp>

using namespace cv;

bool comparateContours(std::vector<Point> c1, std::vector<Point> c2)
{
    return boundingRect(c1).area() > boundingRect(c2).area(); // Compare by area
}

int getRectDist(Rect r1, Rect r2)
{
    return norm(Mat(Point(r1.x, r1.y)), Mat(Point(r2.x, r2.y)));
}

Mat getThreshImg(const Mat &img, const ThreshParams& thresh)
{
    Mat hsv,
        threshImg;

    cvtColor(img, hsv, CV_RGB2HSV);

    inRange(hsv, Scalar(thresh.hMin, thresh.sMin, 0), Scalar(thresh.hMax, thresh.sMax, 255), threshImg);

    return threshImg;
}

int countVideoDevices() {
    CvCapture *cap;
    int n = 1;
    while(1)
    {
        cap = cvCreateCameraCapture(n++);
        if (cap == NULL) break;
        cvReleaseCapture(&cap);
    }

    //cvReleaseCapture(&cap);
    return n-1;
}


Point adjustPnt(Point pnt, Size bounds)
{
    if (pnt.x < 0)
        pnt.x = 0;

    if (pnt.y < 0)
        pnt.y = 0;

    if (pnt.x > bounds.width)
        pnt.x = bounds.width - 1;

    if (pnt.y > bounds.height)
        pnt.y = bounds.height - 1;

    return pnt;
}


double pntDist(Point p1, Point p2)
{
    int dx = p1.x - p2.x,
        dy = p1.y - p2.y;

    return sqrt(dx*dx + dy*dy);
}


bool wrongCardRetFalse(QString msg)
{
    qDebug() << "Card wrong:" << msg;

    return false;
}

void swapSegmentEnds(Vec4i& segment)
{
    std::swap(segment[0], segment[2]);
    std::swap(segment[1], segment[3]);
}


void processVectors(const std::vector<Vec4i> &lines, std::list<std::pair<Vec4i, Vec4i> > &out)
{
    const double MIN_DIST = 100;

    std::list<ParallelGroup> parallelLines;

    for (const Vec4i& vec : lines)
    {
        double angle = getAngle(vec);

        bool group_founded = false;

        for (ParallelGroup& group : parallelLines)
            if (group.isSame(angle))
            {
                group.addVec(vec);
                group_founded = true;

                break;
            }

        if (!group_founded)
        {
            parallelLines.emplace_back(angle);
            parallelLines.back().addVec(vec);
        }
    }

    for (const ParallelGroup& group : parallelLines)
    {
        if (group.getLinesList().size() <= 1)
            continue;

        double minDist = 10e17,
               maxDist = MIN_DIST;

        Vec4i minCandidate,
              maxCandidate;

        for (const Vec4i& vector : group.getLinesList())
        {
            double dist = std::abs(distanceToLine(vector, Point()));

            if (dist < minDist)
            {
                minDist = dist;
                minCandidate = vector;
            }
        }

        for (const Vec4i& vector : group.getLinesList())
        {
            double dist = std::abs(distanceToLine(vector, Point()));

            if (dist > maxDist)
            {
                maxDist = dist;
                maxCandidate = vector;
                minDist = dist;
            }
        }

        if (minCandidate == Vec4i() || maxCandidate == Vec4i())
            continue;

        out.emplace_back(minCandidate, maxCandidate);
    }
}


void drawVec(const Vec2f &vec, Mat &img, const Scalar &color) {
    float rho = vec[0], theta = vec[1];
    Point pt1, pt2;
    double a = cos(theta), b = sin(theta);
    double x0 = a*rho, y0 = b*rho;
    pt1.x = cvRound(x0 + 1000*(-b));
    pt1.y = cvRound(y0 + 1000*(a));
    pt2.x = cvRound(x0 - 1000*(-b));
    pt2.y = cvRound(y0 - 1000*(a));
    cv::line(img, pt1, pt2, color, 3, CV_AA);
}


void drawVec(const Vec4i &vec, Mat &img, const Scalar &color) {
    line(img, Point(vec[0], vec[1]), Point(vec[2], vec[3]), color, 2);
}


double distanceToLine(Point begin, Point end, Point x) {
    //translate the begin to the origin
    end.x -= begin.x;
    end.y -= begin.y;

    x.x -= begin.x;
    x.y -= begin.y;

    //¿do you see the triangle?
    double area = x.x * end.y - x.y * end.x; // cross_product(x, end);
    return area / sqrt(end.x * end.x + end.y * end.y); // norm(end);
}


double distanceToLine(Vec4i line, Point x) {
    return distanceToLine(Point(line[0], line[1]), Point(line[2], line[3]), x);
}


Point intersection(Point p1, Point p2, Point p3, Point p4)
{
    // Store the values for fast access and easy
    // equations-to-code conversion
    float x1 = p1.x, x2 = p2.x, x3 = p3.x, x4 = p4.x;
    float y1 = p1.y, y2 = p2.y, y3 = p3.y, y4 = p4.y;

    float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    // If d is zero, there is no intersection
    if (d == 0)
        return Point();

    // Get the x and y
    float pre = (x1*y2 - y1*x2), post = (x3*y4 - y3*x4);
    float x = ( pre * (x3 - x4) - (x1 - x2) * post ) / d;
    float y = ( pre * (y3 - y4) - (y1 - y2) * post ) / d;

    // Return the point of intersection
    return Point(x, y);
}


Point intersection(Vec4i vec1, Vec4i vec2)
{
    return intersection(Point(vec1[0], vec1[1]), Point(vec1[2], vec1[3]), Point(vec2[0], vec2[1]), Point(vec2[2], vec2[3]));
}


double getAngle(const Vec4i &line) {
    return atan2(line[2] - line[0], line[3] - line[1]);
}


bool closeToHorizontal(double angle) {
    return (angle < CV_PI / 4 && angle > -CV_PI / 4) || (angle > CV_PI * 3 / 4 && angle < CV_PI * 5 / 4);
}


void drawVecIntersection(Vec4i first_vec, Vec4i second_vec, Mat &debug_img) {
    Point i =
            intersection(Point(first_vec[0],  first_vec[1]),  Point(first_vec[2], first_vec[3]),
            Point(second_vec[0], second_vec[1]), Point(second_vec[2], second_vec[3]));

    circle(debug_img, i, 5, Scalar(255, 255, 255), 3);
}


void sortCorners(std::vector<Point2f> &corners, Point center) {
    std::vector<Point2f> top, bot;

    for (unsigned int i = 0; i < corners.size(); i++) {
        if (corners[i].y < center.y)
            top.push_back(corners[i]);
        else
            bot.push_back(corners[i]);
    }

    Point2f tl = top[0].x > top[1].x ? top[1] : top[0];
    Point2f tr = top[0].x > top[1].x ? top[0] : top[1];
    Point2f bl = bot[0].x > bot[1].x ? bot[1] : bot[0];
    Point2f br = bot[0].x > bot[1].x ? bot[0] : bot[1];

    corners[0] = tl;
    corners[1] = tr;
    corners[2] = br;
    corners[3] = bl;
}
