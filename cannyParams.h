#ifndef CANNYPARAMS_H
#define CANNYPARAMS_H

class CannyParams
{
public:
    CannyParams() = delete;
    CannyParams(int threshold1, int threshold2, int approxVal,
                int areaSqrSize = 3) :
        thresh1 (threshold1),
        thresh2 (threshold2),
        areaSize(areaSqrSize), // Optimal value is 3 (increasing kernelSize -> increase contours num)
        approx  (approxVal)
    {

    }

    int thresh1,
        thresh2,
        areaSize,
        approx;
};
#endif // CANNYPARAMS_H
