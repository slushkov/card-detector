#include <iostream>

#ifdef WIN32
#include <windows.h>
#endif
#include <QtCore>

#include "detector.h"
#include "utils.h"
#include "windowsControl.h"

void test_video_captures() {
    int webcam_devices = countVideoDevices();

    std::vector<cv::VideoCapture*> captures;
    for (int i = 0; i < webcam_devices; i++) {
        cv::VideoCapture* c = new cv::VideoCapture(i + 1);
        if (!c->isOpened())
            delete c;
        else
            captures.push_back(c);
    }

    cv::Mat frame;
    while (cv::waitKey(25) == -1) {
        int i = 0;
        for (cv::VideoCapture* capture : captures) {
            capture->read(frame);
            cv::imshow("Capture " + std::string(1, (char) '1' + i++), frame);
        }
    }

    for (cv::VideoCapture* capture : captures)
        delete capture;
}

int main(int argc, char** argv)
{
    #if 0
    if (argc == 2) {
        if (std::string(argv[1]) == "--test_captures" || std::string(argv[1]) == "-t") {
            test_video_captures();
            return 0;
        }
    }
    #endif

    // Params
    int cameraNumber = 0;
    cv::Size limitSize;

    // Analyze command-line arguments
    {
        std::vector<std::string>::iterator iterator;
        std::vector<std::string> args(argc - 1);

        for (int i = 1; i < argc; i++)
            args.push_back(argv[i]);

        if ((iterator = std::find(args.begin(), args.end(), "-c")) != args.end())
            if (iterator != --args.end())
                cameraNumber = ((char)(*++iterator)[0]) - '0';

        if ((iterator = std::find(args.begin(), args.end(), "-l")) != args.end()) {
            if (iterator != --args.end()) {
                if (iterator != --(--args.end())) {
                    int width = atoi((*++iterator).c_str());
                    int height = atoi((*++iterator).c_str());
                    limitSize = cv::Size(width, height);
                }
            }
        }
    }


    Detector detector(cameraNumber, limitSize);
    detector.exec();

    return 0;
}

