#ifndef HAND_DETECT_WINDOWS_CONTROL_H_CODE
#define HAND_DETECT_WINDOWS_CONTROL_H_CODE

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <opencv2/opencv.hpp>

//class WinController
//{
//public:
//    static void itit();
//    static void openClose();
//    static void show(std::string winName)
//};

namespace winNames
{
    extern const std::string Contours,
                             Result,
                             Card,
                             Hough,
                             Threshold ;
} // namespace winNames

namespace winShow
{

extern bool Contours,
            Result,
            Card,
            Hough,
            Threshold;

void keyboardParse();
void openClose(std::string name, bool val);
void openCloseWindows();
void processWindows();
void show(cv::Mat img, std::string title, bool controlVariable);
} // namespace winShow



#endif
