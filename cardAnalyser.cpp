#include "cardAnalyser.h"

#include <QDebug>

using namespace cv;

CardAnalyser::CardAnalyser() :
    tmplMethod_(CV_TM_SQDIFF_NORMED),
    geomDelta_ (20)
{
    logoTmpls_.push_back(imread("logo_MasterCard.png"));
    logoTmpls_.push_back(imread("logo_Visa.png")      );
    chipTmpls_.push_back(imread("chip.png")           );
}

bool CardAnalyser::isCard(Mat cardImg)
{
    cardImg_ = cardImg.clone();

    PaySystem paySystem = findLogo();

    if (paySystem == MASTER_CARD)
        qDebug() << "Pay system : Master Card";
    else if (paySystem == VISA)
        qDebug() << "Pay system : VISA";
    else if (paySystem == UNKNOWN)
        qDebug() << "Pay system : Unknown";

    bool isChip = isChipInPlace();

    qDebug() << (isChip ? "Chip" : "No chip");

    return isChip && paySystem != UNKNOWN;
}

PaySystem CardAnalyser::findLogo()
{
    for (unsigned int i = 0; i < logoTmpls_.size(); ++i)
    {
        Rect logoRect = tmplMatch(cardImg_, logoTmpls_[i], Size(86/20, 54/11));

        if (isLogoBox(logoRect) && logoRect != Rect())
            return i == 0 ? MASTER_CARD : VISA;
    }

    return UNKNOWN;
}

bool CardAnalyser::isChipInPlace()
{
    for (auto tmpl : chipTmpls_)
    {
        Rect chipRect1 = tmplMatch(cardImg_, tmpl, Size(86/11, 54/8 ), true),
             chipRect2 = tmplMatch(cardImg_, tmpl, Size(86/11, 54/11), true);

        if ((isChipBox(chipRect1) && chipRect1 != Rect()) ||
            (isChipBox(chipRect2) && chipRect2 != Rect()))
        {
            return true;
        }
    }

    return false;
}

bool CardAnalyser::isChipBox(Rect box)
{
    return  inGeomDelta(box.x,      double(8)/86 * cardImg_.cols) && inGeomDelta(box.y, double(18)/54 * cardImg_.rows); /*&&
            inGeomDelta(box.width, 11/86 * cardImg_.cols) &&
            (inGeomDelta(box.height, 8/54 * cardImg_.rows) || inGeomDelta(box.height, 11/54 * cardImg_.rows));*/
}

bool CardAnalyser::isLogoBox(Rect box)
{
    return inGeomDelta(box.x,      double(60)/86 * cardImg_.cols); /*&& inGeomDelta(box.width, 25/86 * cardImg_.cols) &&
           inGeomDelta(box.height, 15/54 * cardImg_.rows);*/
}

cv::Rect CardAnalyser::tmplMatch(cv::Mat& cardImg, cv::Mat& tmplFrame, cv::Size reSizeProp, bool isDebug)
{
    Mat corImg,
        tmpl;

    resize(tmplFrame, tmpl, Size(cardImg.cols/reSizeProp.width, cardImg.rows/reSizeProp.height));

    matchTemplate(cardImg, tmpl, corImg, tmplMethod_);
    normalize(corImg, corImg, 0, 1, NORM_MINMAX, -1, Mat());

    double minVal = 0.0,
           maxVal = 0.0;
    Point minLoc,
          maxLoc;

    minMaxLoc(corImg, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

    Point matchLoc = (tmplMethod_ == CV_TM_SQDIFF || tmplMethod_ == CV_TM_SQDIFF_NORMED) ? minLoc : maxLoc;

    if (isDebug)
    {
        Mat resImg = cardImg.clone();

        rectangle(resImg, matchLoc, Point(matchLoc.x + tmpl.cols, matchLoc.y + tmpl.rows), Scalar::all(0), 2, 8, 0);

        imshow("Matching", resImg);
        imshow("Template", tmpl);
    }

    return minVal == 0 ? Rect(matchLoc.x, matchLoc.y, tmpl.cols, tmpl.rows) : Rect();
}
