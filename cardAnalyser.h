#ifndef CARDFINDER_H
#define CARDFINDER_H

#include <opencv2/opencv.hpp>
#include <vector>
#include <QDebug>

enum PaySystem
{
    UNKNOWN,
    MASTER_CARD,
    VISA
};

class CardAnalyser
{
public:
    CardAnalyser();

    bool isCard(cv::Mat cardImg);

private:
    std::vector<cv::Mat> logoTmpls_,
                         chipTmpls_;
    int tmplMethod_,
        geomDelta_;
    cv::Mat cardImg_;

    PaySystem findLogo();
    bool isChipInPlace();

    bool inGeomDelta(double val1, double val2) const { return abs(val1 - val2) < geomDelta_; }
    bool isChipBox(cv::Rect box);
    bool isLogoBox(cv::Rect box);

    cv::Rect tmplMatch(cv::Mat &cardImg, cv::Mat &tmplFrame, cv::Size reSizeProp, bool isDebug = false);
};

#endif // CARDFINDER_H
