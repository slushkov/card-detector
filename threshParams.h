#ifndef THRESHINFO_H
#define THRESHINFO_H

class ThreshParams
{
public:
    ThreshParams() = delete;
    ThreshParams(int hMinVal, int hMaxVal, int sMinVal, int sMaxVal, int minSumVal, int maxSumVal) :
        hMin  (hMinVal),
        hMax  (hMaxVal),
        sMin  (sMinVal),
        sMax  (sMaxVal),
        minSum(minSumVal),
        maxSum(maxSumVal)
    {

    }

    bool isInThresh(int sum)
    {
        return sum > minSum && sum < maxSum;
    }

    int hMin,
        hMax,
        sMin,
        sMax,
        minSum,
        maxSum;
};

#endif // THRESHINFO_H
