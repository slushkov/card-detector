#ifndef MOMENTPARAMS_H
#define MOMENTPARAMS_H

class MomentParams
{
public:
    MomentParams() = delete;
    MomentParams(double momentVal, double deltaVal) :
        nu20 (momentVal),
        delta(deltaVal)
    {

    }

    bool isInRange(double moment)
    {
        return abs(moment - nu20) < delta;
    }

    double nu20,
           delta;
};

#endif // MOMENTPARAMS_H
