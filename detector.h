#ifndef DETECTOR_H
#define DETECTOR_H

#include <opencv2/opencv.hpp>
#include <QDebug>
#include <QtCore>
#include <QMessageBox>

#include "cannyParams.h"
#include "houghParams.h"
#include "geometryParams.h"
#include "threshParams.h"
#include "momentParams.h"
#include "cardAnalyser.h"
#include "utils.h"

#define DEBUG_MSG( msg ) qDebug() << "In function " << __PRETTY_FUNCTION__ << " : " << msg;

class Detector
{
public:
    Detector(const Detector& obj) = delete;
    Detector& operator= (const Detector& obj) = delete;

    Detector();
    Detector(int capture_number, cv::Size limitFrameSize = cv::Size(0, 0));
    ~Detector();

    void detect();
    void exec();

private:
    cv::VideoCapture capture_;
    cv::Mat frame_,
            cannyImg_,
            contoursImg_;
    int fps_;

    // Sets in constructors //
    MomentParams   momentParams_;
    CannyParams    cannyParams_;
    HoughParams    houghParams_;
    GeometryParams geomParams_;
    ThreshParams   threshParams_;
    CardAnalyser cardAnalyzer_;
    cv::Size limitSize_;
    cv::Rect cardRect_;
    std::vector<cv::Mat> logoTempls_;
    cv::Mat chipTmpl_;

    void updateFrames();
    void initTrackbars();
    void getFps();

    bool isCard(const cv::Mat& cardImg, const double& moment);

    std::vector<std::vector<cv::Point> > findContours();
    std::vector<cv::Vec4i> findHoughLines();
    void removeExtraContours();

    cv::Mat getChipImg(const cv::Mat& cardImg);
    cv::Mat getChipThresh(const cv::Mat& cardImg) { return getThreshImg(getChipImg(cardImg), threshParams_); }

    cv::Point findTemplate(const cv::Mat &cardImg, const cv::Mat &tmplFrame, cv::Size reSizeProp);
    bool isLogoInPlace(cv::Mat cardImg);
    bool isChipInPlace(cv::Mat cardImg);

    cv::Mat centralGradient(cv::Mat img, bool fromCenter);

    void drawElements(cv::Mat img, cv::Rect chipRect);

    cv::Mat getCardImg();

};

#endif // DETECTOR_H
