#ifndef GEOMETRYPARAMS_H
#define GEOMETRYPARAMS_H

class GeometryParams
{
public:
    GeometryParams() = delete;
    GeometryParams(int distVal, double deltaAreaVal, double proportionVal,
                   double deltaPropVal) :
        dist           (distVal),
        deltaAreaK     (deltaAreaVal),
        proportions     (proportionVal),
        proportionDelta(deltaPropVal)
    {

    }

    bool isPropInRange(double prop)
    {
        return abs(prop - proportions) < proportionDelta;
    }

    int dist;
    double deltaAreaK,
           proportions,
           proportionDelta;
};

#endif // GEOMETRYPARAMS_H
