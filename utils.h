#ifndef UTILS_H
#define UTILS_H

#include <opencv2/opencv.hpp>
#include <QString>
#include <QDebug>

#include "threshParams.h"

using namespace cv;

int       countVideoDevices();
int       getRectDist      (cv::Rect r1, cv::Rect r2);
bool      wrongCardRetFalse(QString msg);
bool      comparateContours(std::vector<cv::Point> c1, std::vector<cv::Point> c2);
double    pntDist          (cv::Point p1, cv::Point p2);
cv::Mat   getThreshImg     (const cv::Mat& img, const ThreshParams& thresh);
cv::Point adjustPnt        (cv::Point pnt, cv::Size bounds);
void swapSegmentEnds       (cv::Vec4i &segment);


template <class T>
T valInRange (T val, T min, T max)
{
    if (val < min)
        val = min;
    else if (val > max)
        val = max;

    return val;
}

double const EPSILON = CV_PI / 180 * 10;

void drawVec(const Vec2f& vec, Mat& img, const Scalar& color);
void drawVec(const Vec4i& vec, Mat& img, const Scalar& color);

double distanceToLine(Point begin, Point end, Point x);
double distanceToLine(Vec4i line, Point x);

Point intersection(Point p1, Point p2, Point p3, Point p4);
Point intersection(Vec4i vec1, Vec4i vec2);

double getAngle(const Vec4i& line);

bool closeToHorizontal(double angle);

void drawVecIntersection(Vec4i first_vec, Vec4i second_vec, Mat& debug_img);

void sortCorners(std::vector<Point2f>& corners, Point center);

class ParallelGroup
{
public:
    ParallelGroup(double angle) :
        angle_ (angle)
    {
    }

    bool isSame(double angle) const { return std::abs(angle - angle_) < EPSILON; }
    void addVec(const cv::Vec4i& vec) { list_.push_back(vec); }
    const std::list<cv::Vec4i>& getLinesList() const { return list_; }
    std::list<cv::Vec4i>& getLinesList() { return list_; }

private:
    double angle_;
    std::list<cv::Vec4i> list_;
};

void processVectors(const std::vector<cv::Vec4i>& lines, std::list<std::pair<cv::Vec4i, cv::Vec4i>>& out);


#endif
